#from django.shortcuts import render
from django.http import HttpResponse
from random import choice

def hello(request):
	line = choice(('Hello!',
					'Hai there!',
					'Just another test message, nothing serious'))
	message = f"<html><body><h1> {line} </h1></body></html>"
	return HttpResponse(message)

def about(request):
	line_1 = 'This is just a quck-built test project, made for homework'
	line_2 = 'It was made small and light, just to check GitLab services'
	message = f"<html><body><h2> {line_1} <br> {line_2} </h2></body></html>"
	return HttpResponse(message)
